import express, { Application } from 'express';
import cryptRoutes from '../routes/crypt';
import cors from 'cors';



class Server {

    private app: Application;
    private port: string;
    private apiPaths = {
        crypt: '/api/crypt'
    }

    constructor() {
        this.app  = express();
        this.port = process.env.PORT || '8000';

        // Métodos iniciales
        this.middlewares();
        this.routes();
    }


    middlewares() {

        // CORS
        this.app.use( cors() );

        // Lectura del body
        this.app.use( express.json() );

        // Carpeta pública
        this.app.use( express.static('public') );
    }


    routes() {
        this.app.use( this.apiPaths.crypt, cryptRoutes )
    }


    listen() {
        this.app.listen( this.port, () => {
            console.log('Servidor corriendo en puerto ' + this.port );
        })
    }

}

export default Server;