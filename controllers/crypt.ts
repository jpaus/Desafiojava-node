import { Request, Response } from "express";
import { getRandomArbitrary, transpose, capitalize } from "../helpers/utils";


export const postEncrypt = async( req: Request , res: Response ) => {
    const { message } = req.body;
    let messageVector = message.split("");
    let x = Math.floor(getRandomArbitrary(1,99));
    let y = Math.floor(getRandomArbitrary(1,99));
    while (x * y <= message.split("").length) {
        x = Math.floor(getRandomArbitrary(1,99));
        y = Math.floor(getRandomArbitrary(1,99));
    }
    let deltaMessage = x * y - message.split("").length;
    for (let index = 0; index < deltaMessage; index++) {
        //agregar '0' en posiciones randoms
        let randomIndex = Math.floor(getRandomArbitrary(0, messageVector.length));
        messageVector.splice(randomIndex, 0, "0");
        
    }
    //reemplazar '0' por caracteres aleatorios
    messageVector = messageVector.map((element: string) => {
        if (element == "0") {
            return String.fromCharCode(Math.floor(getRandomArbitrary(37,57 ) ) );
        } else {
            return Math.round(getRandomArbitrary(0,1)) === 1 ? element.toUpperCase() : element.toLowerCase();
        }
    })
    //separar arreglo cada 'x' caracteres
    let encryptedMessage = "";
    for (let index = 0; index < messageVector.length; index++) {
        if (index % x == 0 && index != 0) {
            encryptedMessage += "\n";
        }
        encryptedMessage += messageVector[index];
    }
    let encryptedMatrix = transpose(encryptedMessage.split("\n").map((element) => {
        return element.split("");
    }))
    let encryptedMessage2 = encryptedMatrix.map((element: any[]) => {
        return element.join("");
    })
    encryptedMessage = encryptedMessage2.join("\n");

    res.json( encryptedMessage );

}

export const postDecrypt = async( req: Request , res: Response ) => {
    const { message } = req.body;
    let decriptedMessage = "";
    let messageVector = message.split("\n").map((element: string) => element.split(""));
    
    messageVector = transpose(messageVector).map((element) => {
        decriptedMessage += element.filter((element2) => { return element2.match(/[A-Za-z ]/); }).join("").toLowerCase();
    });
    decriptedMessage = capitalize(decriptedMessage).trim();

    res.json( decriptedMessage );
}
