export const getRandomArbitrary = (min: number, max: number) => {
    return Math.random() * (max - min) + min;
  }

export const transpose = (matrix: any[][]) => {
    return matrix[0].map((_: any, column: number) => matrix.map((row: any[]) => row[column]));
}


export const capitalize = (word: string ) => {
  const loweredCase = word.toLowerCase();
  return word[0].toUpperCase() + loweredCase.slice(1);
}
