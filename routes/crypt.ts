import { Router } from 'express';
import { postEncrypt, postDecrypt } from '../controllers/crypt';

const router = Router();


router.post('/encrypt', postEncrypt );
router.post('/decrypt', postDecrypt );



export default router;